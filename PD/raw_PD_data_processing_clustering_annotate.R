#
# Process available PD prefrontal cortext scRNA-seq data from GSM6106340
#
# Direct download from: https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE202210
#


# Need to install Seurat package first
if(!require('Seurat')){

	install.packages('Seurat')
	require('Seurat')
}



# need to set current working diretory containing the data here
setwd('./data_PD/')


# get file names
files = sapply(dir()[grep("mtx",dir())], function(x) strsplit(x,"matrix")[[1]][1])


if(exists("merged"))
	rm(merged)
for(file in files){
	if(!exists("merged")){
		
		
		expression_matrix <- ReadMtx(
	  	mtx = paste(file,"matrix.mtx.gz",sep=""), features = paste(file,"genes.tsv.gz",sep=""),
  		cells = paste(file,"barcodes.tsv.gz",sep="")
		)

		
		print(dim(expression_matrix))
		#  15030   384
		
		# create a Seurat object containing the RNA adata
		merged <- CreateSeuratObject(counts = expression_matrix, assay = "RNA", project=file, min.cells = 3, min.features = 200)

	}else{

		expression_matrix <- ReadMtx(
		  mtx = paste(file,"matrix.mtx.gz",sep=""), features = paste(file,"genes.tsv.gz",sep=""),
  		cells = paste(file,"barcodes.tsv.gz",sep="")
		)
		
		print(dim(expression_matrix))
		
		sobj = CreateSeuratObject(counts = expression_matrix, assay = "RNA", project=file, min.cells = 3, min.features = 200)
		merged <- merge(merged,sobj)
	}
}

save(merged, file="merged.Rdata")
#load(file="merged.Rdata")



#
# Quality control and pre-processing
#


#
# Percentage mitochondrial genes
#

merged[["percent.mt"]] <- PercentageFeatureSet(merged, pattern = "^MT-")

print(head(merged@meta.data, 5))

# mitochondrial genes are present


# create violin plot
pdf("violinplot.pdf")
VlnPlot(merged, features = c("nFeature_RNA", "nCount_RNA", "percent.mt"), ncol = 3)
dev.off()



# We filter cells that have unique feature counts over 10000 or less than 200
# We filter out cells that have >5% mitochondrial counts
dat_filt <- subset(merged, subset = nFeature_RNA > 200 & nFeature_RNA < 10000 & percent.mt < 5)


all(names(dat_filt$orig.ident) == colnames(dat_filt))
#[1] TRUE

head(dat_filt$orig.ident)
#AAACCCAAGTCATCCA-1_1_1_1_1_1_1 AAACCCAGTAATCAGA-1_1_1_1_1_1_1 
#        "GSM6106340_HSDG07HC_"         "GSM6106340_HSDG07HC_" 
#AAACCCATCCATTCGC-1_1_1_1_1_1_1 AAACGAAAGGAGTACC-1_1_1_1_1_1_1 
#        "GSM6106340_HSDG07HC_"         "GSM6106340_HSDG07HC_" 
#AAACGAACATGAAGCG-1_1_1_1_1_1_1 AAACGAAGTGCCTACG-1_1_1_1_1_1_1 
#        "GSM6106340_HSDG07HC_"         "GSM6106340_HSDG07HC_" 


# assign condition labels: PD = Parkinson's disease, HC = healthy control
cond_labs = rep("HC",length(dat_filt$orig.ident))
cond_labs[grep("PD_",dat_filt$orig.ident)] = rep("PD",length(grep("PD_",dat_filt$orig.ident)))

table(cond_labs)
#cond_labs
#   HC    PD 
#29865 35555



# Apply normalization
sctransv2 = SCTransform(dat_filt, vst.flavor = "v2")


# get most variable features
top20 <- head(VariableFeatures(sctransv2), 20) # already included implicitly in SCTransform function
top20


# create feature plots
plot1 <- VariableFeaturePlot(sctransv2)
plot2 <- LabelPoints(plot = plot1, points = top20, repel = TRUE)

pdf("variable_feature_plot_v2.pdf")
	LabelPoints(plot = plot1, points = top20, repel = TRUE)
dev.off()


# Run dimension reduction and clustering
sctransv2 <- RunPCA(sctransv2, npcs = 30, verbose = FALSE) # only 30 PCs used in next analysis
sctransv2 <- RunUMAP(sctransv2, reduction = "pca", dims = 1:30, verbose = FALSE) # reduction = "pca" is default parameter

sctransv2 <- FindNeighbors(sctransv2, reduction = "pca", dims = 1:30, verbose = FALSE)

#
# Clustering
#

# resolution: Value of the resolution parameter, use a value above (below) 1.0 if you want to obtain a larger (smaller) number of communities. default = 0.8

sctransv2 <- FindClusters(sctransv2, resolution = 0.8, verbose = FALSE) 

pdf("dimplot_umap_v2.pdf")
DimPlot(sctransv2, label = TRUE) + NoLegend()
dev.off()




#
# Automated selection of the number of clusters
#
# following: https://hbctraining.github.io/scRNA-seq/lessons/elbow_plot_metric.html#:~:text=View%20on%20GitHub-,Elbow%20plot%3A%20quantitative%20approach,the%20majority%20of%20the%20variation.


# Determine percent of variation associated with each PC
pct <- sctransv2[["pca"]]@stdev / sum(sctransv2[["pca"]]@stdev) * 100

# Calculate cumulative percents for each PC
cumu <- cumsum(pct)

# Determine which PC exhibits cumulative percent greater than 90% and % variation associated with the PC as less than 5
co1 <- which(cumu > 90 & pct < 5)[1]

print(co1)

#The first metric returns the number of PCs matching these requirements. Lets check the second metric, which identifies the PC where the percent change in variation between consecutive PCs is less than 0.1%:

# Determine the difference between variation of PC and subsequent PC
co2 <- sort(which((pct[1:length(pct) - 1] - pct[2:length(pct)]) > 0.1), decreasing = T)[1] + 1

# last point where change of % of variation is more than 0.1%.
print(co2)

#This second metric returns another number of PCs. Usually, we would choose the minimum of these two metrics as the PCs covering the majority of the variation in the data.

# Minimum of the two calculations
pcs <- min(co1, co2)

print(pcs)
# 15



#
# Alternative approach: Use Silhouette width cluster validity index
#


# Make silhoutte plots at different clustering resolutions (of seurat) to identify best cluster number/size
library(cluster)

reduction <- "pca"
dims <- 1:pcs
dist.matrix <- dist(x = Embeddings(object = sctransv2[[reduction]])[, dims])

# test possible clustering resolutions
resolution <- c(0.001, 0.005, 0.01, 0.02, 0.03, 0.04, 0.05, 0.06, 0.07, 0.08, 0.1, 0.2, 0.3, 0.4, 0.5)

silhoutte_width <- data.frame()
# par(mfrow=c(4,5))
# png("silhoutte_width_plots.png", width=8, height=6, units="in", res=300)
for (i in 1:length(resolution)) {
	print(resolution[i])
	sctransv2 <- FindClusters(sctransv2, resolution = resolution[i])
	clusters <- eval(parse(text = paste0("sctransv2$SCT_snn_res.",resolution[i])))
	sil <- silhouette(x = as.numeric(x = as.factor(x = clusters)), dist = dist.matrix)
	#plot(sil, main=resolution[i]) # silhoutte_5_cluster_res0.01 # Average silhoutte width: 0.63
	
	if(length(table(clusters)) < 2)
		next
	
	print(mean(sil[, "sil_width"])) # 0.5256185
	result <- cbind(resolution[i], length(unique(clusters)), mean(sil[, "sil_width"]))
	colnames(result) <- c("resolution", "clusters", "sil_width")
	silhoutte_width <- rbind(silhoutte_width, result)
}
# dev.off

print(silhoutte_width)

# check for which number of clusters the maximum silhouette with is obtained (e.g., for 11 clusters)


pdf("silhoutte_width.pdf")
ggplot(silhoutte_width, aes(x = clusters, y = sil_width)) +
  geom_line() + geom_point() + scale_x_continuous(breaks = 2:31) + theme_bw()
dev.off()

# e.g., if best result is at "0.04" resolution, set the variable accordingly:
bestres = 0.04

# apply find clusters with the chosen resolution setting
sctransv2 <- FindClusters(sctransv2, resolution = bestres)


# set condition labels
sctransv2$cond <- cond_labs


# create low-dimensional plot for selected number of clusters, split by condition
pdf("DimPlot_umap_selres.pdf")
DimPlot(sctransv2, reduction = "umap", split.by = "cond")
dev.off()




#
# Automated cell-type mapping and annotation using SCTypeDB
#

# load libraries and functions
#lapply(c("dplyr","Seurat","HGNChelper"), library, character.only = T)
system('wget https://raw.githubusercontent.com/IanevskiAleksandr/sc-type/master/R/gene_sets_prepare.R')
system('wget https://raw.githubusercontent.com/IanevskiAleksandr/sc-type/master/R/sctype_score_.R')

source("gene_sets_prepare.R");
source("sctype_score_.R")


# DB file
system('wget https://raw.githubusercontent.com/IanevskiAleksandr/sc-type/master/ScTypeDB_full.xlsx')
#db_ = "https://raw.githubusercontent.com/IanevskiAleksandr/sc-type/master/ScTypeDB_full.xlsx";
db_ = "ScTypeDB_full.xlsx";
tissue = "Brain" # e.g. Immune system, Liver, Pancreas, Kidney, Eye, Brain


if(!require('openxlsx')){

	install.packages('openxlsx')
	require('openxlsx')
}


# get cell marker database gene lists per cell type
cellmarker = read.xlsx('Cell_marker_Human_Cellmarker_Brain_08-12-2022.xlsx',1, startRow=1)


gspositive_lst = cellmarker$Symbol
names(gspositive_lst) = cellmarker$cell_name

# filter by removing missing values
gspositive_lst = gspositive_lst[which(!is.na(gspositive_lst))]

# merge named vectors with duplicate names
gspositive_combined = tapply(unlist(gspositive_lst, use.names = FALSE), rep(names(gspositive_lst), lengths(gspositive_lst)), FUN = c)
gspositive_combined  = sapply(gspositive_combined,function(x) unique(x))

# final length of annotated gene sets
print(length(gspositive_combined))

sum(gspositive_combined$`Truncated radial glial cell`=="CRYAB")

gspositive_combined$`Truncated radial glial cell`

# Run cell type annotation scoring
es.max_cellmarker = sctype_score(scRNAseqData = sctransv2[["SCT"]]@scale.data, scaled = TRUE, gs = gspositive_combined, gs2 = NULL)
dim(es.max_cellmarker)

cL_resutls_cellmarker = do.call("rbind", lapply(unique(sctransv2@meta.data$seurat_clusters), function(cl){
		#print(cl)
    es.max.cl = sort(rowSums(es.max_cellmarker[ ,rownames(sctransv2@meta.data[sctransv2@meta.data$seurat_clusters==cl, ])]), decreasing = !0)
    head(data.frame(cluster = cl, type = names(es.max.cl), scores = es.max.cl, ncells = sum(sctransv2@meta.data$seurat_clusters==cl)), 10)
}))


# show top-ranked annotation results for first cluster (cluster number 0)
head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 0),], 10)

# show top-ranked annotation results for 2nd cluster (cluster number 1)
head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 1),], 10)

# etc. for other clusters

head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 2),], 10)


head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 3),], 10)


head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 4),], 10)

head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 5),], 10)

head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 6),], 10)

head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 7),], 10)

head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 8),], 10)


head(cL_resutls_cellmarker[which(cL_resutls_cellmarker$cluster == 9),], 10)




# Determine top assignments for each cluster (set unreliable assignments to "Unknown")
if(!require('tidyverse')){

	install.packages('tidyverse')
	require('tidyverse')
}
sctype_scores_cellmarker = cL_resutls_cellmarker %>% group_by(cluster) %>% top_n(n = 1, wt = scores)

sctype_scores_cellmarker$type[as.numeric(as.character(sctype_scores_cellmarker$scores)) < sctype_scores_cellmarker$ncells/4] = "Unknown"

print(sctype_scores_cellmarker[,1:3])


# transfer the annotation to the data
sctransv2@meta.data$classcellmarker = ""
for(j in unique(sctype_scores_cellmarker$cluster)){
  cl_type = sctype_scores_cellmarker[sctype_scores_cellmarker$cluster==j,]; 
  sctransv2@meta.data$classcellmarker[sctransv2@meta.data$seurat_clusters == j] = as.character(cl_type$type[1])
}

# plot the final annotated cell type clusters with labels
pdf("umap_plot_celltype_annot_cellmarker2.pdf")
DimPlot(sctransv2, reduction = "umap", label = TRUE, repel = TRUE, group.by = 'classcellmarker')  
dev.off()


save(sctransv2,file="sctransv2_pd_annotated.Rdata")#save the annotated seurat object will be used to compute the markers


##########Visualize markers inside clusters#######



#Create feature plot for specific marker you want to visualize (e.g article:SLC1A2","SYT1","PLP1","LHFPL3","B2M" )

for (gene in c("SLC1A2","SYT1","PLP1","LHFPL3","B2M")){
  
  png(paste0("Visualize_marker_cluster_AD_",gene,".png"))
  print(FeaturePlot(sctransv2, features =  gene, pt.size =
                      0.2, min.cutoff = "q9", ncol=2))
  dev.off()
  
}

