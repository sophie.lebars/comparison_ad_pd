# Comparison_AD_PD

Pipeline to process and annotate single-cell dataset. Comparison of two single-cell datasets on different biological levels such as gene, pathway, and cellular sub-network.




## AD folder

Find the code to process the AD single-cell dataset.

> "raw_AD_data_processing_clustering_annotate.R"

Aim: Process the raw AD data and do Seurat cluster with bestres=0.07 (clustree &silhouette) and annotation with SCtype approach

Input: raw AD single-cell data, "Cell_marker_Human_Cellmarker_Brain_08-12-2022.xlsx" for annotation

Output: seurat object with seurat cluster in "sctransv2_ad_annotated.Rdata", "violin_plot_raw_ad.pdf", "umap_plot_celltype_annot_cellmarker2_raw_ad_merged.pdf", "silhoutte_width_plots_ad.pdf"


> "find_markers_AD.R"


Aim: Find markers for all cell types between AD vs HC, find Top3 markers for all clusters, find markers between AD vs HC for a specific cell type (e.g. Astrocyte, Neuron, Microglial)

Input: "sctransv2_ad_annotated.Rdata"

Output:'differential_expression_ad_vs_hc_all_celltypes_logFC.xlsx', "Top3_marker_cluster_ad.csv", 'differential_expression_ad_vs_hc_microglial_logFC.xlsx', differential_expression_ad_vs_hc_neuron_logFC.xlsx', differential_expression_ad_vs_hc_astrocyte_logFC.xlsx'




## PD folder

same script as for (#AD) and same output ad -> pd

## Comparison 


>"disease_specific_genes_function.R"


Aim: find the disease-specific DEGs and the shared DEG with and without the same  direction


Input: DEGs (differentially expressed genes) AD, DEGs PD resulting from "find_marker_X.R", target FDR, exclude p-value, minimum logFC

Output: AD-specific DEG, PD-specific DEG, shared DEG, different DEG (DEG)


>"find_disease-specific-shared-genes.R"

Aim: See the overlapping DEG as a Venn diagram, search for the DEG referenced as neuroprotective, and conduct the pathway analyses to search for each categorie of DEGs (specific, shared, different) in which molecular function or biological process is overrepresented. call "disease_specific_genes_function.R"

Input: "differential_expression_ad_vs_hc_all_X_logFC.xlsx", "differential_expression_pd_vs_hc_all_X_logFC.xlsx", X=cell type (e.g. all_celltype, neuron, microglial, astrocyte)

Output: Venn_diagramm- "diag_X.svg," intersection of DEGs which are neuroprotective, overrepresented biological process (BP) and molecular function (MF), Y=MF or BP- "Top_Y_DEG_X.png"

#Differential_Network_Analysis_Zickenrott_etal_2016

The network analyses with the Zickenrott pipeline

## Installation

All the needed R packages can be found in SessionInfo.txt in #AD folder and #Comparison folder.

## Contact

sophie.lebars@uni.lu


## Authors

Sophie Le Bars and Enrico Glaab

## License
For open source projects, say how it is licensed.


